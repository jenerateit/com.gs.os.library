<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.gs.os</groupId>
	<artifactId>com.gs.os.library-maven</artifactId>
	<version>1.1.0</version>
	<packaging>pom</packaging>
	<name>gApp 3rd. party libraries</name>

	<developers>
		<developer>
			<id>mmt</id>
			<name>Marcus Munzert</name>
			<email>marcus.munzert@generative-software.com</email>
			<url>https://www.xing.com/profile/Marcus_Munzert</url>
			<organization>Generative Software GmbH</organization>
			<organizationUrl>http://www.generative-software.com</organizationUrl>
			<timezone>(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</timezone>
			<roles>
				<role>architect developer</role>
			</roles>
		</developer>
	</developers>

	<licenses>
		<license>
			<name>Eclipse Public License (EPL)</name>
			<url>http://www.eclipse.org/org/documents/epl-v10.html</url>
		</license>
	</licenses>

	<properties>
		<tycho-version>1.2.0</tycho-version>
		<tycho-extras-version>${tycho-version}</tycho-extras-version>
	</properties>

	<profiles>
		<profile>
			<id>neon</id>
			<activation>
				<activeByDefault>false</activeByDefault>
			</activation>

			<modules>
				<module>com.gs.os.library-target-neon/</module>
			</modules>

			<build>
				<plugins>
					<plugin>
						<groupId>org.eclipse.tycho</groupId>
						<artifactId>target-platform-configuration</artifactId>
						<version>${tycho-version}</version>
						<configuration>
							<target>
								<artifact>
									<groupId>com.gs.os</groupId>
									<artifactId>com.gs.os.library-target</artifactId>
									<version>1.1.0.neon</version>
								</artifact>
							</target>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

	<modules>
		<module>io.swagger</module>
		<module>io.swagger.feature</module>
		
		<module>org.apache.poi</module>
		<module>org.apache.poi.feature</module>
		
		<module>okhttp3</module>
                <module>okhttp3.logging-interceptor</module>
		<module>okio</module>
		<module>okhttp3.feature</module>
		
		<module>retrofit2</module>
		<module>retrofit2.converter-jackson</module>
		<module>retrofit2.feature</module>
		
		<module>plexus.utils</module>
		<module>maven.model</module>
		<module>maven.feature</module>

		<module>com.gs.os.library-repository</module>
	</modules>

	<build>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<excludes>
					<exclude>**/rebel.xml</exclude>
					<exclude>**/.gitignore</exclude>
				</excludes>
			</resource>
		</resources>

		<plugins>
			<!-- Tycho for OSGI bundles and Eclipse plugins -->
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-versions-plugin</artifactId>
				<version>${tycho-version}</version>
				<configuration>
					<newVersion>${project.version}</newVersion>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-maven-plugin</artifactId>
				<version>${tycho-version}</version>
				<extensions>true</extensions>
			</plugin>
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-compiler-plugin</artifactId>
				<version>${tycho-version}</version>
				<configuration>
					<excludeResources>
						<excludeResource>**/rebel.xml</excludeResource>
						<excludeResource>**/.gitignore</excludeResource>
					</excludeResources>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<!-- add this plugin to have a reference repository which is needed to 
				check the tycho-packaging jgit dependencies -->
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-p2-plugin</artifactId>
				<version>${tycho-version}</version>
				<configuration>
					<baselineRepositories>
						<repository>
							<url>https://developer.virtual-developer.com/p2/com.gs.os.library/</url>
						</repository>
					</baselineRepositories>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-packaging-plugin</artifactId>
				<version>${tycho-version}</version>
				<dependencies>
					<dependency>
						<groupId>org.eclipse.tycho.extras</groupId>
						<artifactId>tycho-buildtimestamp-jgit</artifactId>
						<version>${tycho-extras-version}</version>
					</dependency>
				</dependencies>
				<configuration>
					<timestampProvider>jgit</timestampProvider>
					<jgit.ignore>
						pom.xml
					</jgit.ignore>
					<jgit.dirtyWorkingTree>warning</jgit.dirtyWorkingTree>
				</configuration>
			</plugin>
		</plugins>

		<pluginManagement>
			<plugins>
                                <plugin>
                                        <groupId>org.eclipse.tycho</groupId>
                                        <artifactId>tycho-surefire-plugin</artifactId>
                                        <version>${tycho-version}</version>
                                <configuration>
					<!-- Java memory parameter settings -->
					<argLine>-Xms1G -Xmx2G</argLine>
                                        <!-- Uncomment to connect remote debugger -->
                                        <!-- <debugPort>5000</debugPort> -->

                                        <!-- Uncomment to debug with "telnet localhost 5555" to OSGI console -->
                                    	<!-- <appArgLine>-consoleLog -console 5555 -noExit</appArgLine> -->

                                        <defaultStartLevel>
                                                <level>4</level>
                                                <autoStart>true</autoStart>
                                        </defaultStartLevel>

                                        <dependencies>
                                                <dependency>
                                                        <type>p2-installable-unit</type>
                                                        <artifactId>org.eclipse.equinox.ds</artifactId>
                                                </dependency>
                                                <dependency>
                                                        <type>p2-installable-unit</type>
                                                        <artifactId>org.eclipse.equinox.cm</artifactId>
                                                </dependency>
                                                <dependency>
                                                        <type>p2-installable-unit</type>
                                                        <artifactId>org.eclipse.equinox.console</artifactId>
                                                </dependency>
                                                <dependency>
                                                        <type>p2-installable-unit</type>
                                                        <artifactId>org.apache.felix.gogo.shell</artifactId>
                                                </dependency>
                                                <dependency>
                                                        <type>p2-installable-unit</type>
                                                        <artifactId>org.apache.felix.gogo.command</artifactId>
                                                </dependency>
                                                <dependency>
                                                        <type>p2-installable-unit</type>
                                                        <artifactId>org.apache.felix.gogo.runtime</artifactId>
                                                </dependency>
                                                <!-- <dependency>
                                                        <type>p2-installable-unit</type>
                                                        <artifactId>slf4j-simple</artifactId>
                                                </dependency> -->
                                        </dependencies>

                                        <bundleStartLevel>
                                                <bundle>
                                                        <id>javax.transaction</id>
                                                        <level>4</level>
                                                        <autoStart>false</autoStart>
                                                </bundle>
                                                <bundle>
                                                        <id>org.eclipse.tycho.surefire.junit4</id>
                                                        <level>4</level>
                                                        <autoStart>false</autoStart>
                                                </bundle>
                                        </bundleStartLevel>
                                </configuration>
                        	</plugin>
                        </plugins>
		</pluginManagement>
	</build>

	<organization>
		<name>Generative Software GmbH</name>
		<url>http://www.generative-software.de/</url>
	</organization>
</project>
