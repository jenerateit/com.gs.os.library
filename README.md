# Open Source Libraries for Virtual Developer

[![Eclipse License](https://img.shields.io/badge/license-Eclipse-brightgreen.svg)](https://www.eclipse.org/legal/epl-v10.html)
[![Build Status](https://img.shields.io/bitbucket/pipelines/jenerateit/com.gs.os.library/master.svg)](https://bitbucket.org/jenerateit/com.gs.os.library/addon/pipelines/home)


[![Virtual Developer](https://www.virtual-developer.com/wp-content/uploads/2018/01/VD-logo.png)](https://www.virtual-developer.com/)


### Overview

Rebundled Libraries for Virtual Developer.


### Getting Started with Virtual Developer

Check out the Virtual Developer [Homepage](https://www.virtual-developer.com/) and [Wiki](https://docs.virtual-developer.com/technical-documentation/) for information. Don't hesitate to [contact](https://www.virtual-developer.com/kontakt/) us.


### Installation

#### P2 Update Site

Install from this repository into your target: [https://developer.virtual-developer.com/p2/com.gs.os.library/](https://developer.virtual-developer.com/p2/com.gs.os.library/)  


### License

Published under the [Eclipse Public License 1.0](https://www.eclipse.org/legal/epl-v10.html) 



### Provided by

[![Generative Software GmbH](https://www.virtual-developer.com/wp-content/uploads/2011/06/2011-05-17-IMG-GenerativeSoft-Logo-480x134-e1307098095106-300x83.png)](https://www.generative-software.com/)
